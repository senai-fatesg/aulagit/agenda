import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'a-pessoa',
  templateUrl: './pessoa.component.html',
  styleUrls: ['./pessoa.component.css']
})
export class PessoaComponent implements OnInit {

  pessoa = {"nome": "", "endereco": "", "telefone": ""};
  pessoas:any = [];

  constructor() { }

  ngOnInit(): void {
  }

  salvar(){
    this.pessoas.push(this.pessoa);
    this.pessoa = {"nome": "", "endereco": "", "telefone": ""}
  }

}
