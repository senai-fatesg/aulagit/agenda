import { Component } from '@angular/core';

@Component({
  selector: 'a-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'agenda';

  nome = "Jose";

  salvar(){
    console.log(this.nome);
  }

}
